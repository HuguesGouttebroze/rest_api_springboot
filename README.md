# REST API - Spring Boot

* L’idée est de gérer des employés

* **l’API** devra offrir un **CRUD** (Create, Read, Update, Delete) pour les données des employés.

* Les données seront dans une base de données **H2** (*H2 est une base de données relationnelle Java très légère, qui par défaut fonctionne en **in memory** (au démarrage du programme, la structure de la base est construite, puis qd le programme s’arrête, le contenu de la base de données est supprimé, les modifications apportées d’une exécution à l’autre du programme ne sont pas persistées)*).

* Notre **API** devra donc exposer des endpoints correspondant aux actions du **CRUD**, et communiquer avec la base de données pour récupérer ou modifier les informations des employés. 

* À noter que **l’API** sera de type **REST**.

## Starters

* Pour les dépendances :

  * **Spring Web** (permet de faire du RESTful, ce qui correspond à notre API pour exposer des endpoints).

  * **Lombok** (librarie d'optimisation des classes).

  * **H2 Database** (pr faire du H2).

  * **Spring Data JPA** (permet de gérer la persistance des données avec la base de données).

## Contrôleur REST pour la gestion des données

## Tester notre API avec Spring Boot

* **objectif**:
  * tester les controllers, simuler un appel à travers des URL.
  * exécuter des requêtes sur notre controller. 
  * voir annotation **@WebMvcTest**.

### Ecrire nos tests unitaires avec @WebMvcTest

* ``@WebMvcTest`` permet d’écrire des tests unitaires sur le controller.

* On vérifie uniquement le code exécuté dans la méthode du controller en mockant les appels au service.

```java
...
@WebMvcTest(controllers = EmployeeController.class)
public class EmployeeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmployeeService employeeService;

    @Test
    public void testGetEmployees() throws Exception {
        mockMvc.perform(get("/employees"))
            .andExpect(status().isOk());
    }
}
```

* `@WebMvcTest(controllers = EmployeeController.class)` déclenche le mécanisme permettant de tester les controllers (on indique également le ou les controllers concernés).

* L’attribut `mockMvc` permet d’appeler la méthode `perform` qui déclenche la requête.

* L’attribut `employeeService` est annoté ``@MockBean`` (obligatoire, car la méthode du controller exécutée par l’appel de ``/employees`` utilise cette classe).

* La méthode ``perform`` prend en paramètre l’instruction ``get(“/employees”)``. On exécute donc une requête **GET** sur **l’URL** ``/employees``.

* L’instruction ``.andExpect(status().isOk())`` indique que nous attendons une réponse HTTP 200.

### Modélisation de la table en entité JAVA

* Pr manipuler les données persistées dans la base de données, ns créons une classe **Java** qui est une **entité**.
  * la classe représente la table. 
  * chaque ligne de la table correspondra à une instance de la classe. 
  
* Créons la classe **Employee** dans le package `fr.hgouttebroze.api.model`

### Implémentez la communication entre l’application et la base de données

* Après avoir créé l'entité, il faut implémenter le code qui déclenche les actions pour communiquer avec la base de données. Ce code se servira de notre classe entité.

* **Principe**: 
  - *le code fait une requête à la base de données*
  - *le résultat nous est retourné sous forme d’instances de l’objet **Employee**.*

* Le code à implémenter est une **interface**

* 1 interface ne contient pas de code, pr exécuter des requêtes ... mais le  composant **Spring Data JPA** nous permet d’exécuter ces requêtes **SQL**, sans avoir besoin de les écrire.



