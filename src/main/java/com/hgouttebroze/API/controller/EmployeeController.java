package com.hgouttebroze.API.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hgouttebroze.API.model.Employee;
import com.hgouttebroze.API.service.EmployeeService;

@RestController
public class EmployeeController {
/*
 * l'annotation @RestController indique à Spring:
 * 		- que cette classe est un Bean, comm l annotation @Component
 * 		- d'insérer le retour de la méthode au format JSON ds le corps de la réponse HTTP 
 * 		(ainsi, les appli. communicant avec notre API accédent au résultat de leur requête en parsant la réponse HTTP).
 */
	
	@Autowired
	private EmployeeService employeeService; 
		/* l'injection de cette instance EmployeeService permettra 
		 * d'appeller les méthodes pr communiquer avec la BDD 
		 */
	
	/**
	 * 
	 * Create - Add a new employee
	 * @param employee
	 * @return The employee object saved
	 * 
	 */
	@PostMapping("/employee")
	public Employee createEmployee(@RequestBody Employee employee) {
		return employeeService.saveEmployee(employee);
	}
	
	/** 
	 * Read - Get one employee
	 * @param id
	 * @return An Employee object full filled
	 *
	 */ 
	@GetMapping("/employee/{id}")
	public Employee getEmployee(@PathVariable("id") final Long id) {
		Optional<Employee> employee = employeeService.getEmployee(id);
		if(employee.isPresent()) {
			return employee.get();
		} else {
			return null;
		}
	}
	
	/**
	 * Read - Get all employees
	 * @return - an Iterable object of Employee full filled
	 */
	@GetMapping("/employees")
	public Iterable<Employee> getEmployees() {
		//les requêtes HTTP de type GET à l'URL /employees executeront le code de cette méthode
		return employeeService.getEmployees();
		/* on appel la méthode getEmployees() du service, 
		 * ce dernier appellera la méthode findAll() du repository, 
		 * ce qui ns retourne ts les employés enregistrés en BDD 
		 */
	}
	
	/**
	 * Update - Update an existing employee
	 * @param id - the id of the employee to update
	 * @param employee - the employee object updated
	 * @return
	 */
	@PutMapping("/employee/{id}")
	public Employee updateEmployee(@PathVariable("id") final Long id, @RequestBody Employee employee) {
		Optional<Employee> e = employeeService.getEmployee(id);
		if(e.isPresent()) {
			Employee currentEmployee = e.get();
			
			String firstName = employee.getFirstName();
			if(firstName != null) {
				currentEmployee.setFirstName(firstName);
			}
			
			String lastName = employee.getLastName();
			if(lastName != null) {
				currentEmployee.setLastName(lastName);
			}
			
			String mail = employee.getMail();
			if(mail != null) {
				currentEmployee.setMail(mail);
			}
			
			String password = employee.getPassword();
			if(password != null) {
				currentEmployee.setPassword(password);
			}
			employeeService.saveEmployee(currentEmployee);
			return currentEmployee;		
		} else {
			return null;
		}
	}
	
	/**
	 * Delete - Delete an employee
	 * @param id - the id of the employee to delete
	 */
	@DeleteMapping("/employee/{id}")
	public void deleteEmployee(@PathVariable("id") final Long id) {
		employeeService.deleteEmployee(id);
	}
	
}

/*
 * MEMO des annotations de type HTTP
 * 
 * ANNOTATION    Type HTTP  OBJECTIF
 * 
 * @GetMapping 		GET 	pr la lecture de données
 * 
 * @PostMapping 	POST 	pr l'envoi de données (pr créer un nouvel 
 * 							élément par ex.)
 * 
 * @PatchMapping 	PATCH 	pr la MAJ partielle de l'élément envoyé
 * 
 * @PutMapping 		PUT 	pr le remplacement complet de l'élément envoyé
 * 
 * @DeleteMapping 	DELETE  pr la suppression de l'élément envoyé
 * 
 * @RequestMapping  -----   Intègre ts les types HTTP. Le type souhaité est 
 * 						 	indiqué comme attribut de l'annotation. 
 * 						 	Exemple: 
 * 						 	@RequestMapping(method = RequestMethod.GET)
 */
