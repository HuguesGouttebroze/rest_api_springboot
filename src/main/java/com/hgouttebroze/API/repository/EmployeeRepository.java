package com.hgouttebroze.API.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hgouttebroze.API.model.Employee;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Long> {
	

}
