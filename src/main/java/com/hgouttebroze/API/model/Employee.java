package com.hgouttebroze.API.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import lombok.Data;


@Entity
@Data
@Table(name = "employees")
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	private String mail;
	
	private String password;

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setFirstName(String firstName2) {
		// TODO Auto-generated method stub
		
	}

	public String getLastName() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setLastName(String lastName2) {
		// TODO Auto-generated method stub
		
	}
}

/*
 * A propos des annotations:
 * 
 * 	- @Data: annotation Lombok (librairie "Lombok" automatise l'ajout 
 * 	des getters et setters, ce qui allége le code).
 * 
 * 	- @Entity: annotation indique que la classe correspond à une table 
 * 	de la base de données.
 * 
 * 	- @Table(name=”employees”) indique le nom de la table associée.
 * 
 * 	- @Id: L’attribut id correspond à la clé primaire de la table, 
 * 	et est donc annoté @Id. D’autre part, comme l’id est auto-incrémenté, 
 * 	ns ajoutons l’annotation: 
 * 		@GeneratedValue(strategy = GenerationType.IDENTITY).
 * 
 * 	-@Column: firstName et lastName sont annotés avec @Column pour 
 * 	faire le lien avec le nom du champ de la table.
 * 
 */